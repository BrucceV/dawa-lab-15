import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'appAngular';
  nombre = 'Brucce Villena';
  edad = '19';
  email = 'brucce.villena@tecsup.edu.pe'
  sueldos = [1700, 1600, 1900];
  activo = true;
  sitio = 'http://www.google.com';
  profesion = '';
  anios = '';
}
